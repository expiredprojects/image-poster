package rainhead.com.controls;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 17.05.13
 * Time: 23:41
 * To change this template use File | Settings | File Templates.
 */
public abstract class MyControls extends JPanel implements ActionListener{
    protected static final Color myBackground = new Color(100, 100, 100);
    protected static final Color myForeground = new Color(240, 240, 240);

    public MyControls(){
        super();
        setBackground(myBackground);
        this.setLayout(new GridBagLayout());
    }
}
