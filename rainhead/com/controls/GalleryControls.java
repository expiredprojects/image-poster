package rainhead.com.controls;

import rainhead.com.render.GalleryRender;
import rainhead.utils.RainingImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 20.05.13
 * Time: 11:27
 * To change this template use File | Settings | File Templates.
 */
public class GalleryControls extends MyControls {
    public static final int myWidth = 100;
    public static final int myHeight = 50;

    private JButton loadButton;
    private JFileChooser fileChooser;

    private GalleryRender galleryDraw;

    public GalleryControls(GalleryRender _galleryDraw){
        super();
        setPreferredSize(new Dimension(myWidth, myHeight));
        galleryDraw = _galleryDraw;

        initialize();
    }

    void initialize() {
        loadButton = new JButton("Load Image");
        fileChooser = new JFileChooser("C:\\Users\\Mc\\Pictures");

        skinElements();
        addElements();
        listenElements();
    }

    void skinElements() {
        loadButton.setBackground(myBackground);
        loadButton.setForeground(myForeground);
    }

    void addElements() {
        this.add(loadButton, new GridBagConstraints());
    }

    void listenElements() {
        loadButton.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source.equals(loadButton)){
            int result = fileChooser.showOpenDialog(GalleryControls.this);

            if (result == JFileChooser.APPROVE_OPTION){
                File file = fileChooser.getSelectedFile();
                System.out.println(fileChooser.getSelectedFile().getAbsolutePath());
                try {
                    BufferedImage bufferedImage = ImageIO.read(file);
                    RainingImage rainingImage = new RainingImage(bufferedImage, fileChooser.getSelectedFile().getAbsolutePath());
                    if (bufferedImage != null){
                        galleryDraw.importImage(rainingImage);
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }
}
