package rainhead.com.controls;

import rainhead.com.render.Canvas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 20.05.13
 * Time: 12:09
 * To change this template use File | Settings | File Templates.
 */
public class BottomControls extends MyControls {
    public static final int myWidth = 800;
    public static final int myHeight = 50;

    public static final String MODE_TOP = "Top";
    public static final String MODE_BOTTOM = "Bottom";

    public static final String MODE_LEFT = "Left";
    public static final String MODE_UP = "Up";
    public static final String MODE_DOWN = "Down";
    public static final String MODE_RIGHT = "Right";

    public static final String MODE_R_PLUS = "Rotate+";
    public static final String MODE_R_MINUS = "Rotate-";

    public static final String MODE_STORE = "Store";
    public static final String MODE_LOAD = "Load";

    public static final String MODE_EXPORT = "Export";


    private JButton[] jButtons;

    private Canvas canvas;

    public BottomControls(Canvas _canvas){
        super();
        setPreferredSize(new Dimension(myWidth, myHeight));

        canvas = _canvas;
        initialize();
    }

    void initialize() {
        jButtons = new JButton[11];

        jButtons[0] = new JButton(MODE_TOP);
        jButtons[1] = new JButton(MODE_BOTTOM);

        jButtons[2] = new JButton(MODE_LEFT);
        jButtons[3] = new JButton(MODE_UP);
        jButtons[4] = new JButton(MODE_DOWN);
        jButtons[5] = new JButton(MODE_RIGHT);

        jButtons[6] = new JButton(MODE_R_PLUS);
        jButtons[7] = new JButton(MODE_R_MINUS);

        jButtons[8] = new JButton(MODE_STORE);
        jButtons[9] = new JButton(MODE_LOAD);

        jButtons[10] = new JButton(MODE_EXPORT);

        skinElements();
        addElements();
        listenElements();
    }

    void skinElements() {
        for (int i = 0; i < jButtons.length; i++){
            jButtons[i].setBackground(myBackground);
            jButtons[i].setForeground(myForeground);
        }
    }

    void addElements() {
        for (int i = 0; i < jButtons.length; i++){
            this.add(jButtons[i], new GridBagConstraints());
        }
    }

    void listenElements() {
        for (int i = 0; i < jButtons.length; i++){
            jButtons[i].addActionListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source instanceof JButton){
            String mode = ((JButton) source).getText();

            if (mode == MODE_TOP || mode == MODE_BOTTOM){
                canvas.orderFocus(mode);
            }
            else if ((mode == MODE_LEFT || mode == MODE_UP || mode == MODE_DOWN || mode == MODE_RIGHT) ||
                    (mode == MODE_R_PLUS || mode == MODE_R_MINUS)){
                canvas.modAffine(mode);
            }
            else if (mode == MODE_STORE){
                canvas.saveState();
            }
            else if (mode == MODE_LOAD){
                canvas.loadState();
            }
            else if (mode == MODE_EXPORT){
                canvas.exportImage();
            }
        }
    }

    public void hideComment(){
    /*
    ec-change of the elements order (bring to top, push to bottom),
    mc-fine tuning of an element position and rotation angle with pushbuttons,
    ec-store/load operations of all affine transforms, so as all results of work with a poster can be stored in the text file and then retrieved,
    ec-the ability to store the image in a graphic file of arbitrary resolution (not limited by the actual screen window resolution).
    */
    }
}
