package rainhead.com.listener;

import rainhead.com.render.Canvas;
import rainhead.utils.RainingImage;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 22.05.13
 * Time: 17:15
 * To change this template use File | Settings | File Templates.
 */
public class Savior {
    public static final String RELEASE_CODE = "release";

    private Shape ghostShape;
    private RainingImage ghostImage;
    private Point ghostPoint;

    private Canvas canvas;
    private boolean bContained = false;

    public void uploadRenderers(Canvas c){
        canvas = c;
    }

    public void callGhost(Object ghost){
        if (ghost instanceof Shape){
            ghostShape = (Shape) ghost;
            System.out.println(ghostShape);
        }
        else if (ghost instanceof RainingImage){
            ghostImage = (RainingImage) ghost;
            System.out.println(ghostImage);
        }
        else if (ghost instanceof Point){
            ghostPoint = (Point) ghost;
            if (canvas.getBounds().contains(ghostPoint)){
                bContained = true;
                ghostPoint = transformGhost(ghostPoint);
                if (ghostShape != null && ghostShape instanceof Rectangle2D.Double){
                    ((Rectangle2D.Double) ghostShape).x = ghostPoint.getX();
                    ((Rectangle2D.Double) ghostShape).y = ghostPoint.getY();
                    canvas.ghostShape = ghostShape;
                }
                else if (ghostShape != null && ghostShape instanceof Ellipse2D.Double){
                    ((Ellipse2D.Double) ghostShape).x = ghostPoint.getX();
                    ((Ellipse2D.Double) ghostShape).y = ghostPoint.getY();
                    canvas.ghostShape = ghostShape;
                }
                else if (ghostImage != null){
                    canvas.ghostPoint = ghostPoint;
                    canvas.ghostImage = ghostImage;
                }
                canvas.repaint();
            }
            else {
                bContained = false;
            }
        }
        else if (ghost == Savior.RELEASE_CODE){
            if (bContained){
                canvas.materialize();
                System.out.println("Success!");
            }
            else {
                System.out.println("Out of canvas!");
            }
            bContained = false;
            ghostShape = null;
            ghostImage = null;
            ghostPoint = null;
            canvas.cleanSavior();
            canvas.repaint();
        }
    }

    private Point transformGhost(Point point) {
        if (ghostShape != null && ghostShape instanceof Rectangle2D.Double){
            return new Point((int) (point.getX() - canvas.getX() - ((Rectangle2D.Double) ghostShape).width / 2),
                    (int) (point.getY() - canvas.getY() - ((Rectangle2D.Double) ghostShape).height / 2));
        }
        else if (ghostShape != null && ghostShape instanceof Ellipse2D.Double){
            return new Point((int) (point.getX() - canvas.getX() - ((Ellipse2D.Double) ghostShape).width / 2),
                    (int) (point.getY() - canvas.getY() - ((Ellipse2D.Double) ghostShape).height / 2));
        }
        else if (ghostImage != null){
            return new Point((int) (point.getX() - canvas.getX() - ghostImage.getBufferedImage().getWidth() / 2),
                    (int) (point.getY() - canvas.getY() - ghostImage.getBufferedImage().getHeight() / 2));
        }
        return new Point((int) (point.getX() - canvas.getX()),
                (int) (point.getY() - canvas.getY()));
    }
}
