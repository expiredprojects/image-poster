package rainhead.com.actors;

import rainhead.utils.RainingImage;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 28.05.13
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
public class Item {
    private Object render;
    private Point anchor;
    private Rectangle2D bounds;
    private Rectangle2D updatedBounds;
    private Node[] nodes;   //0-3r 4sy 5-6sx 7sy 8t

    public AffineTransform[] affineTransforms;  //0ren 1tr 2ro 3sx 4rx 5sy 6ry
    public String path = null;

    public Item(Object r, Point a){
        if (r instanceof RainingImage){
            path = ((RainingImage) r).getPath();
            render = ((RainingImage) r).getBufferedImage();
            System.out.println(path);
        }
        else {
            render = r;
        }
        anchor = a;
        initAffines();
        initialize();
    }

    public Item(Object r, Point a, AffineTransform[] at){
        if (r instanceof RainingImage){
            path = ((RainingImage) r).getPath();
            render = ((RainingImage) r).getBufferedImage();
        }
        else {
            render = r;
        }
        anchor = a;
        affineTransforms = at;
        initialize();
    }

    private void initialize() {
        initBounds();
        initNodes();
        updateSelf();
    }

    private void initAffines() {
        affineTransforms = new AffineTransform[7];
        for (int i = 0; i < affineTransforms.length; i++){
            affineTransforms[i] = new AffineTransform();
        }
    }

    private void initBounds() {
        if (render instanceof Shape) {
            bounds = ((Shape) render).getBounds();
        }
        else if (render instanceof BufferedImage){
            bounds = new Rectangle2D.Double(anchor.getX(), anchor.getY(),
                    ((BufferedImage) render).getWidth(), ((BufferedImage) render).getHeight());
        }
    }

    private void initNodes() {
        nodes = new Node[9];

        nodes[0] = new Node(Node.TYPE_ROTATE, bounds.getX(), bounds.getY());
        nodes[1] = new Node(Node.TYPE_ROTATE, bounds.getX() + bounds.getWidth(), bounds.getY());
        nodes[2] = new Node(Node.TYPE_ROTATE, bounds.getX(), bounds.getY() + bounds.getHeight());
        nodes[3] = new Node(Node.TYPE_ROTATE, bounds.getX() + bounds.getWidth(), bounds.getY() + bounds.getHeight());

        nodes[4] = new Node(Node.TYPE_SCALE_Y, bounds.getX() + (bounds.getWidth() / 2), bounds.getY());

        nodes[5] = new Node(Node.TYPE_SCALE_X, bounds.getX(), bounds.getY() + (bounds.getHeight() / 2));
        nodes[6] = new Node(Node.TYPE_SCALE_X, bounds.getX() + bounds.getWidth(), bounds.getY() + (bounds.getHeight() / 2));

        nodes[7] = new Node(Node.TYPE_SCALE_Y, bounds.getX() + (bounds.getWidth() / 2), bounds.getY() + bounds.getHeight());

        nodes[8] = new Node(Node.TYPE_TRANSLATE, bounds.getX() + (bounds.getWidth() / 2), bounds.getY() + (bounds.getHeight() / 2));
    }

    public void updateSelf() {
        updatedBounds = affineTransforms[0].createTransformedShape(bounds).getBounds();
    }

    public Object getRender(){
        return render;
    }

    public Rectangle2D getBounds(){
        return bounds;
    }

    public Rectangle2D getUpdatedBounds(){
        return updatedBounds;
    }

    public Node[] getNodes(){
        return nodes;
    }

    public void debugAffines(){
        for (int i = 0; i < affineTransforms.length; i++){
            System.out.println(affineTransforms[i]);
        }
    }
}
