package rainhead.com.actors;

import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 28.05.13
 * Time: 20:26
 * To change this template use File | Settings | File Templates.
 */
public class Node {
    private static final int FIXED_LENGTH = 10;

    public static final String TYPE_ROTATE = "rotate";
    public static final String TYPE_SCALE_X = "scale_x";
    public static final String TYPE_SCALE_Y = "scale_y";
    public static final String TYPE_TRANSLATE = "translate";

    private String currentType;
    private Rectangle2D rectangle2D;

    public Node(String t, double x, double y){
        currentType = t;
        rectangle2D = new Rectangle2D.Double(x - FIXED_LENGTH / 2, y - FIXED_LENGTH / 2, FIXED_LENGTH, FIXED_LENGTH);
    }

    public String getCurrentType(){
        return currentType;
    }

    public Rectangle2D getRectangle2D(){
        return rectangle2D;
    }
}
