package rainhead.com;

import rainhead.com.listener.Savior;
import rainhead.com.controls.BottomControls;
import rainhead.com.controls.GalleryControls;
import rainhead.com.render.*;
import rainhead.com.render.Canvas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 17.05.13
 * Time: 23:38
 * To change this template use File | Settings | File Templates.
 */
public class MyWindow extends JFrame{
    private static final int GALLERY_SCROLL = 18;

    private Container container;
    private Container innerWest;
    private Container innerGallery;
    private JScrollPane scrollPane;

    private Savior savior;
    private Canvas canvas;
    private GalleryRender galleryRender;
    private ShapeRender shapeRender;

    private JFrame _this;

    public MyWindow(){
        super("Image POSTER");
        _this = this;

        savior = new Savior();
        container = this.getContentPane();

        buildContainer();
        buildSavior();

        this.setVisible(true);
        this.addWindowStateListener(new WindowStateListener() {
            @Override
            public void windowStateChanged(WindowEvent e) {
                if (e.getOldState() == 1 || e.getOldState() == 7){
                    if (canvas != null) canvas.dispatchEvent(new ComponentEvent(_this, ComponentEvent.COMPONENT_RESIZED));
                }
            }
        });
    }

    private void buildSavior() {
        savior.uploadRenderers(canvas);
    }

    private void buildContainer() {
        buildWest();
        container.add(innerWest, BorderLayout.WEST);

        canvas = new Canvas();
        canvas.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);    //To change body of overridden methods use File | Settings | File Templates.
                if (canvas != null) canvas.repaint();
                System.out.println("Resize!");
            }
        });
        container.add(canvas, BorderLayout.CENTER);

        BottomControls bottomControls = new BottomControls(canvas);
        container.add(bottomControls, BorderLayout.SOUTH);
    }

    private void buildWest() {
        innerWest = new Container();
        innerWest.setLayout(new BorderLayout());

        buildGallery();
        innerWest.add(innerGallery, BorderLayout.CENTER);

        shapeRender = new ShapeRender(savior);
        innerWest.add(shapeRender, BorderLayout.SOUTH);
    }

    private void buildGallery() {
        innerGallery = new Container();
        innerGallery.setLayout(new BorderLayout());

        galleryRender = new GalleryRender(savior);

        GalleryControls galleryControls = new GalleryControls(galleryRender);
        innerGallery.add(galleryControls, BorderLayout.NORTH);

        buildScroll(galleryRender);
        innerGallery.add(scrollPane, BorderLayout.CENTER);
    }

    private void buildScroll(GalleryRender galleryRender) {
        scrollPane = new JScrollPane(galleryRender, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        JScrollBar scrollBar = scrollPane.getVerticalScrollBar();
        scrollBar.setPreferredSize(new Dimension(GALLERY_SCROLL, 0));
    }
}
