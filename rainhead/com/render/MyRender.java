package rainhead.com.render;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 21.05.13
 * Time: 20:34
 * To change this template use File | Settings | File Templates.
 */
public abstract class MyRender extends JPanel implements MouseInputListener{
    protected static final Color myBackground = new Color(100, 100, 100);
    protected static final Color myForeground = new Color(200, 200, 200);

    protected Graphics2D graphics2D;

    public MyRender(){
        super();
        setBackground(myBackground);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
