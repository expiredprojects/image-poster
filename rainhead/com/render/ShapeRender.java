package rainhead.com.render;

import rainhead.com.listener.Savior;
import rainhead.utils.SpringUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 20.05.13
 * Time: 11:48
 * To change this template use File | Settings | File Templates.
 */
public class ShapeRender extends MyRender {
    public static final int myWidth = 100;
    public static final int myHeight = 200;

    private static final int eWidth = 64;
    private static final int eHeight = 64;

    private Shape[] shapes;
    private JLabel[] labels;

    private Savior savior;
    private boolean bContained = false;

    public ShapeRender(Savior s){
        super();
        setPreferredSize(new Dimension(myWidth, myHeight));
        setLayout(new SpringLayout());
        savior = s;

        initialize();
    }

    private void initialize() {
        shapes = new Shape[2];
        labels = new JLabel[2];

        shapes[0] = generateRectangle();
        shapes[1] = generateEllipse();

        labels[0] = new JLabel(generateImage(shapes[0]));
        labels[1] = new JLabel(generateImage(shapes[1]));

        setupElements();
        addElements();

        SpringUtilities.makeCompactGrid(this, 2, 1, (myWidth - eWidth) / 2, (myHeight - eHeight) / 6, 0, (myHeight - eHeight) / 6);
    }

    private Rectangle2D.Double generateRectangle(){
        return new Rectangle2D.Double(0, 0, eWidth, eHeight);
    }

    private Ellipse2D.Double generateEllipse(){
        return new Ellipse2D.Double(0, 0, eWidth, eHeight);
    }

    private Icon generateImage(Shape shape) {
        BufferedImage bufferedImage = new BufferedImage(eWidth, eHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        g2d.setColor(myForeground);
        g2d.fill(shape);
        g2d.dispose();
        Icon icon = new ImageIcon(bufferedImage);
        return icon;
    }

    private void setupElements() {
        for (int i = 0; i < labels.length; i++){
            labels[i].setSize(new Dimension(eWidth, eHeight));
            labels[i].setLocation(0, 0);
        }
    }

    private void addElements() {
        for (int i = 0; i < labels.length; i++){
            this.add(labels[i]);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        for (int i = 0; i < labels.length; i++){
            if (labels[i].getBounds().contains(e.getPoint())){
                if (shapes[i] instanceof Rectangle2D){
                    savior.callGhost(generateRectangle());
                    bContained = true;
                }
                else if (shapes[i] instanceof Ellipse2D){
                    savior.callGhost(generateEllipse());
                    bContained = true;
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (bContained){
            bContained = false;
            savior.callGhost(Savior.RELEASE_CODE);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (bContained){
            savior.callGhost(new Point(this.getX() + e.getX(), this.getY() + e.getY()));
        }
    }
}
