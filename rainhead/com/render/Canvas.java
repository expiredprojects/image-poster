package rainhead.com.render;

import rainhead.com.actors.Item;
import rainhead.com.actors.Node;
import rainhead.com.controls.BottomControls;
import rainhead.utils.RainingImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 22.05.13
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
public class Canvas extends MyRender {
    private static final Color canvasBackground = new Color(240, 240, 240);

    private static final String FILE_NAME = "save_state.txt";
    private static final String ASSEMBLE_FINAL = "null";

    private ArrayList<Item> itemArrayList;
    private Item focusItem = null;
    private Node focusNode = null;

    public Shape ghostShape;
    public RainingImage ghostImage;
    public Point ghostPoint;

    private AffineTransform blankAffine = new AffineTransform();
    private AffineTransform ghostAffine = new AffineTransform();
    private AffineTransform ghostRepair = new AffineTransform();

    private boolean bDragOmitted;
    private Rectangle focusBounds;

    public Canvas(){
        super();
        setBackground(canvasBackground);

        itemArrayList = new ArrayList<Item>();
    }

    public void paintComponent(Graphics graphics){
        super.paintComponent(graphics);
        graphics2D = (Graphics2D) graphics;
        drawObjects();
        drawGhost();
        drawFocus();
    }

    private void drawObjects() {
        graphics2D.setColor(myForeground);
        if (itemArrayList != null){
            for (int i = 0; i < itemArrayList.size(); i++){
                graphics2D.setTransform(itemArrayList.get(i).affineTransforms[0]);
                if (itemArrayList.get(i).getRender() instanceof Shape){
                    graphics2D.fill((Shape) itemArrayList.get(i).getRender());
                }
                else if (itemArrayList.get(i).getRender() instanceof BufferedImage){
                    BufferedImage bufferedImage = (BufferedImage) itemArrayList.get(i).getRender();
                    graphics2D.drawImage(bufferedImage, (int) itemArrayList.get(i).getBounds().getX(), (int) itemArrayList.get(i).getBounds().getY(), null);
                }
            }
            graphics2D.setTransform(blankAffine);
        }
    }

    private void drawGhost() {
        graphics2D.setColor(myForeground);
        if (ghostShape != null){
            graphics2D.draw(ghostShape);
        }
        else if (ghostImage != null && ghostPoint != null){
            graphics2D.drawImage(ghostImage.getBufferedImage(), (int) ghostPoint.getX(), (int) ghostPoint.getY(), null);
        }
    }

    private void drawFocus() {
        graphics2D.setColor(myBackground);
        if (focusItem != null){
            graphics2D.setTransform(focusItem.affineTransforms[0]);
            for (int i = 0; i < focusItem.getNodes().length; i++){
                graphics2D.draw(focusItem.getNodes()[i].getRectangle2D());
            }
            graphics2D.setTransform(blankAffine);
        }
    }

    public void materialize(){
        if (ghostShape != null){
            itemArrayList.add(new Item(ghostShape, null));
        }
        else if (ghostImage != null && ghostPoint != null){
            itemArrayList.add(new Item(ghostImage, ghostPoint));
        }
    }

    public void cleanSavior(){
        ghostShape = null;
        ghostImage = null;
        ghostPoint = null;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == 3 && focusItem != null){
            itemArrayList.remove(itemArrayList.indexOf(focusItem));
            focusItem = null;
            repaint();
            return;
        }

        if (focusItem != null){
            for (int i = 0; i < focusItem.getNodes().length; i++){
                if (focusItem.affineTransforms[0].createTransformedShape(focusItem.getNodes()[i].getRectangle2D()).contains(e.getPoint())){
                    focusNode = focusItem.getNodes()[i];
                    focusBounds = focusItem.affineTransforms[0].createTransformedShape(focusNode.getRectangle2D()).getBounds();
                    bDragOmitted = true;
                    return;
                }
            }
        }

        for (int i = itemArrayList.size() - 1; i > -1; i--){
            if (itemArrayList.get(i).getUpdatedBounds().contains(e.getPoint())){
                focusItem = itemArrayList.get(i);
                repaint();
                return;
            }
        }

        System.out.println("Clear focus.");
        focusItem = null;
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (focusNode != null && focusItem != null){
            if (!bDragOmitted){
                updateAffine(focusNode.getCurrentType());
                assembleAffine(ASSEMBLE_FINAL);
                focusItem.updateSelf();
            }
            cleanAffine();
            repaint();
        }
    }

    private void updateAffine(String mode) {
        if (mode.equals(Node.TYPE_TRANSLATE)){
            focusItem.affineTransforms[1].concatenate(ghostAffine);
        }
        else if (mode.equals(Node.TYPE_ROTATE)){
            focusItem.affineTransforms[2].concatenate(ghostAffine);
        }
        else if (mode.equals(Node.TYPE_SCALE_X)){
            focusItem.affineTransforms[3] = new AffineTransform(ghostAffine);
            focusItem.affineTransforms[4] = new AffineTransform(ghostRepair);
        }
        else if (mode.equals(Node.TYPE_SCALE_Y)){
            focusItem.affineTransforms[5] = new AffineTransform(ghostAffine);
            focusItem.affineTransforms[6] = new AffineTransform(ghostRepair);
        }
    }

    private void assembleAffine(String mode) {
        focusItem.affineTransforms[0] = new AffineTransform();

        focusItem.affineTransforms[0].concatenate(focusItem.affineTransforms[1]);
        if (mode.equals(Node.TYPE_TRANSLATE)) focusItem.affineTransforms[0].concatenate(ghostAffine);

        focusItem.affineTransforms[0].concatenate(focusItem.affineTransforms[2]);
        if (mode.equals(Node.TYPE_ROTATE)) {
            focusItem.affineTransforms[0].concatenate(ghostAffine);
        }

        if (mode.equals(Node.TYPE_SCALE_X)) focusItem.affineTransforms[0].concatenate(ghostRepair);
        else focusItem.affineTransforms[0].concatenate(focusItem.affineTransforms[4]);

        if (mode.equals(Node.TYPE_SCALE_X)) focusItem.affineTransforms[0].concatenate(ghostAffine);
        else focusItem.affineTransforms[0].concatenate(focusItem.affineTransforms[3]);

        if (mode.equals(Node.TYPE_SCALE_Y)) focusItem.affineTransforms[0].concatenate(ghostRepair);
        else focusItem.affineTransforms[0].concatenate(focusItem.affineTransforms[6]);

        if (mode.equals(Node.TYPE_SCALE_Y)) focusItem.affineTransforms[0].concatenate(ghostAffine);
        else focusItem.affineTransforms[0].concatenate(focusItem.affineTransforms[5]);
    }

    private void cleanAffine() {
        ghostAffine.setToIdentity();
        ghostRepair.setToIdentity();
        focusNode = null;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (focusNode == null) return;
        bDragOmitted = false;

        if (focusNode.getCurrentType().equals(Node.TYPE_TRANSLATE)){
            ghostAffine.setToTranslation(e.getX() - focusBounds.getCenterX(), e.getY() - focusBounds.getCenterY());
        }
        else if (focusNode.getCurrentType().equals(Node.TYPE_ROTATE)){
            double atan1 = Math.atan2(e.getY() - focusItem.getUpdatedBounds().getCenterY(), e.getX() - focusItem.getUpdatedBounds().getCenterX());
            double atan2 = Math.atan2(focusBounds.getY() - focusItem.getUpdatedBounds().getCenterY(), focusBounds.getX() - focusItem.getUpdatedBounds().getCenterX());
            ghostAffine.setToRotation(atan1 - atan2, focusItem.getBounds().getCenterX(), focusItem.getBounds().getCenterY());
        }
        else if (focusNode.getCurrentType().equals(Node.TYPE_SCALE_X)){
            double scaleX = Math.abs(e.getX() - focusItem.getUpdatedBounds().getCenterX()) * 2 / focusItem.getBounds().getWidth();
            double scaleY = Math.abs(e.getY() - focusItem.getUpdatedBounds().getCenterY()) * 2 / focusItem.getBounds().getWidth();
            if (scaleX < scaleY) scaleX = scaleY;
            ghostRepair.setToTranslation(-1 * (focusItem.getBounds().getCenterX() * scaleX - focusItem.getBounds().getCenterX()), 0);
            ghostAffine.setToScale(scaleX, 1);
        }
        else if (focusNode.getCurrentType().equals(Node.TYPE_SCALE_Y)){
            double scaleX = Math.abs(e.getX() - focusItem.getUpdatedBounds().getCenterX()) * 2 / focusItem.getBounds().getHeight();
            double scaleY = Math.abs(e.getY() - focusItem.getUpdatedBounds().getCenterY()) * 2 / focusItem.getBounds().getHeight();
            if (scaleY < scaleX) scaleY = scaleX;
            ghostRepair.setToTranslation(0, -1 * (focusItem.getBounds().getCenterY() * scaleY - focusItem.getBounds().getCenterY()));
            ghostAffine.setToScale(1, scaleY);
        }

        assembleAffine(focusNode.getCurrentType());
        repaint();
    }

    public void orderFocus(String mode){
        if (focusItem == null) return;

        int currentIndex = itemArrayList.indexOf(focusItem);
        ArrayList<Item> ghostCopyList = new ArrayList<Item>();

        if (mode == BottomControls.MODE_BOTTOM){
            ghostCopyList.add(focusItem);
        }

        for (int i = 0; i < itemArrayList.size(); i++){
            if (i != currentIndex){
                ghostCopyList.add(itemArrayList.get(i));
            }
        }

        if (mode == BottomControls.MODE_TOP){
            ghostCopyList.add(focusItem);
        }

        itemArrayList = ghostCopyList;
        repaint();
    }

    public void modAffine(String mode){
        if (focusItem == null) return;

        if (mode == BottomControls.MODE_LEFT){
            ghostAffine.setToTranslation(-10, 0);
        }
        else if (mode == BottomControls.MODE_UP){
            ghostAffine.setToTranslation(0, -10);
        }
        else if (mode == BottomControls.MODE_DOWN){
            ghostAffine.setToTranslation(0, 10);
        }
        else if (mode == BottomControls.MODE_RIGHT){
            ghostAffine.setToTranslation(10, 0);
        }
        else if (mode == BottomControls.MODE_R_PLUS){
            ghostAffine.setToRotation(Math.PI * 0.1, focusItem.getBounds().getCenterX(), focusItem.getBounds().getCenterY());
        }
        else if (mode == BottomControls.MODE_R_MINUS){
            ghostAffine.setToRotation(-Math.PI * 0.1, focusItem.getBounds().getCenterX(), focusItem.getBounds().getCenterY());
        }
        if (mode == BottomControls.MODE_LEFT || mode == BottomControls.MODE_UP ||
                mode == BottomControls.MODE_DOWN || mode == BottomControls.MODE_RIGHT){
            updateAffine(Node.TYPE_TRANSLATE);
        }
        else if (mode == BottomControls.MODE_R_PLUS || mode == BottomControls.MODE_R_MINUS){
            updateAffine(Node.TYPE_ROTATE);
            System.out.println(focusItem.affineTransforms[2]);
        }

        assembleAffine(ASSEMBLE_FINAL);
        focusItem.updateSelf();

        cleanAffine();
        repaint();
    }

    public void saveState(){
        try {
            FileWriter fileWriter = new FileWriter(FILE_NAME);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (int i = 0; i < itemArrayList.size(); i++){
                focusItem = itemArrayList.get(i);

                bufferedWriter.write(Integer.toString(i));
                bufferedWriter.newLine();

                if (focusItem.getRender() instanceof Rectangle2D) bufferedWriter.write("Rectangle2D");
                else if (focusItem.getRender() instanceof Ellipse2D) bufferedWriter.write("Ellipse2D");
                else if (focusItem.getRender() instanceof BufferedImage) bufferedWriter.write(focusItem.path);
                bufferedWriter.newLine();

                bufferedWriter.write(Double.toString(focusItem.getBounds().getX()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.getBounds().getY()));
                bufferedWriter.newLine();

                bufferedWriter.write(Double.toString(focusItem.getBounds().getWidth()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.getBounds().getHeight()));
                bufferedWriter.newLine();

                bufferedWriter.write(Double.toString(focusItem.affineTransforms[1].getTranslateX()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.affineTransforms[1].getTranslateY()));
                bufferedWriter.newLine();

                bufferedWriter.write(Double.toString(focusItem.affineTransforms[2].getTranslateX()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.affineTransforms[2].getTranslateY()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.affineTransforms[2].getShearX()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.affineTransforms[2].getShearY()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.affineTransforms[2].getScaleX()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.affineTransforms[2].getScaleY()));
                bufferedWriter.newLine();

                bufferedWriter.write(Double.toString(focusItem.affineTransforms[3].getScaleX()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.affineTransforms[4].getTranslateX()));
                bufferedWriter.newLine();

                bufferedWriter.write(Double.toString(focusItem.affineTransforms[5].getScaleY()));
                bufferedWriter.newLine();
                bufferedWriter.write(Double.toString(focusItem.affineTransforms[6].getTranslateY()));
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
            focusItem = null;
            System.out.println("Saved!");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void loadState(){
        try {
            ArrayList<Item> itemSaveList = new ArrayList<Item>();
            Object render;
            Point anchor;

            focusItem = null;

            String currentLine;
            FileReader fileReader = new FileReader(FILE_NAME);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while (bufferedReader.readLine() != null){
                currentLine = bufferedReader.readLine();

                double x = Double.parseDouble(bufferedReader.readLine());
                double y = Double.parseDouble(bufferedReader.readLine());
                anchor = new Point((int) x, (int) y);

                double w = Double.parseDouble(bufferedReader.readLine());
                double h = Double.parseDouble(bufferedReader.readLine());

                if (currentLine.equals("Rectangle2D")){
                    render = new Rectangle2D.Double(x, y, w, h);
                }
                else if (currentLine.equals("Ellipse2D")){
                    render = new Ellipse2D.Double(x, y, w, h);
                }
                else {
                    System.out.println(currentLine);
                    render = new RainingImage(ImageIO.read(new File(currentLine)), currentLine);
                }

                AffineTransform[] affineTransforms = new AffineTransform[7];
                AffineTransform at = new AffineTransform();
                affineTransforms[0] = new AffineTransform();

                x = Double.parseDouble(bufferedReader.readLine());
                y = Double.parseDouble(bufferedReader.readLine());
                at.setToTranslation(x, y);
                affineTransforms[1] = new AffineTransform(at);

                x = Double.parseDouble(bufferedReader.readLine());
                y = Double.parseDouble(bufferedReader.readLine());
                w = Double.parseDouble(bufferedReader.readLine());
                h = Double.parseDouble(bufferedReader.readLine());
                double sx = Double.parseDouble(bufferedReader.readLine());
                double sy = Double.parseDouble(bufferedReader.readLine());
                at.setTransform(sx, h, w, sy, x, y);
                affineTransforms[2] = new AffineTransform(at);

                sx = Double.parseDouble(bufferedReader.readLine());
                at.setToScale(sx, 1);
                affineTransforms[3] = new AffineTransform(at);

                x = Double.parseDouble(bufferedReader.readLine());
                at.setToTranslation(x, 0);
                affineTransforms[4] = new AffineTransform(at);

                sy = Double.parseDouble(bufferedReader.readLine());
                at.setToScale(1, sy);
                affineTransforms[5] = new AffineTransform(at);

                y = Double.parseDouble(bufferedReader.readLine());
                at.setToTranslation(0, y);
                affineTransforms[6] = new AffineTransform(at);

                itemSaveList.add(new Item(render, anchor, affineTransforms));

                focusItem = itemSaveList.get(itemSaveList.size() - 1);
                assembleAffine(ASSEMBLE_FINAL);
                focusItem.updateSelf();
                focusItem = null;
            }
            bufferedReader.close();
            focusItem = null;
            itemArrayList = itemSaveList;
            repaint();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void exportImage(){
        System.out.println("Specify x and y for export resolution.");
        Scanner scanner = new Scanner(System.in);
        System.out.println("X:");
        int x = scanner.nextInt();
        System.out.println("Y:");
        int y = scanner.nextInt();

        BufferedImage bufferedImage = new BufferedImage(x, y, BufferedImage.TYPE_INT_RGB);
        graphics2D = bufferedImage.createGraphics();
        graphics2D.setColor(canvasBackground);
        graphics2D.fillRect(0, 0, x, y);
        drawObjects();
        try {
            ImageIO.write(bufferedImage, "PNG", new File("render.png"));
            System.out.println("Render complete!");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
