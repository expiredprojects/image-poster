package rainhead.com.render;

import rainhead.com.listener.Savior;
import rainhead.utils.RainingImage;
import rainhead.utils.SpringUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 21.05.13
 * Time: 20:51
 * To change this template use File | Settings | File Templates.
 */
public class GalleryRender extends MyRender {
    public static final int myWidth = 80;
    public static final int myHeight = 80;

    private ArrayList<RainingImage> rainingImageArrayList;
    private ArrayList<JLabel> labelArrayList;

    private Savior savior;
    private boolean bContained = false;

    public GalleryRender(Savior s){
        super();
        setLayout(new SpringLayout());
        savior = s;

        initialize();
    }

    private void initialize() {
        rainingImageArrayList = new ArrayList<RainingImage>();
        labelArrayList = new ArrayList<JLabel>();
    }

    public void paintComponent(Graphics graphics){
        super.paintComponent(graphics);
        graphics2D = (Graphics2D) graphics;

        rebuildSize();
    }

    private void rebuildSize() {
        for (int i = 0; i < labelArrayList.size(); i++){
            labelArrayList.get(i).setSize(new Dimension(myWidth, myHeight));
        }
    }

    public void importImage(RainingImage rainingImage){
        rainingImageArrayList.add(rainingImage);
        labelArrayList.add(new JLabel(new ImageIcon(rainingImage.getBufferedImage().getScaledInstance(myWidth, myHeight, Image.SCALE_SMOOTH)),
                JLabel.TRAILING));
        JLabel lastLabel = labelArrayList.get(labelArrayList.size() - 1);
        lastLabel.setSize(new Dimension(myWidth, myHeight));
        lastLabel.setLocation(0, (labelArrayList.size() - 1) * myHeight);
        this.add(lastLabel);

        SpringUtilities.makeCompactGrid(this, labelArrayList.size(), 1, 0, 0, 0, 0);
        repaint();
        getParent().revalidate();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        for (int i = 0; i < rainingImageArrayList.size(); i++){
            int x = labelArrayList.get(i).getX();
            int y = labelArrayList.get(i).getY();
            boolean conditionX = x < e.getX() && (x + myWidth) > e.getX();
            boolean conditionY = y < e.getY() && (y + myHeight) > e.getY();
            if (conditionX && conditionY){
                savior.callGhost(rainingImageArrayList.get(i));
                bContained = true;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (bContained){
            bContained = false;
            savior.callGhost(Savior.RELEASE_CODE);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (bContained){
            savior.callGhost(new Point(this.getParent().getParent().getX() + e.getX(), this.getParent().getParent().getY() + e.getY()));
        }
    }
}
