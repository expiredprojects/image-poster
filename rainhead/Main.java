package rainhead;

import rainhead.com.controls.BottomControls;
import rainhead.com.controls.GalleryControls;
import rainhead.com.render.GalleryRender;
import rainhead.com.render.ShapeRender;
import rainhead.com.MyWindow;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 17.05.13
 * Time: 23:35
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    private static final int GALLERY_OFFSET = 20;

    public static void main(String[] args) {
        MyWindow myWindow = new MyWindow();
        myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myWindow.setVisible(true);

        myWindow.pack();
        setMinimumSize(myWindow);
    }

    private static void setMinimumSize(MyWindow myWindow) {
        Insets insets = myWindow.getInsets();
        myWindow.setMinimumSize(new Dimension(BottomControls.myWidth + insets.right + insets.left,
                GalleryControls.myHeight + GalleryRender.myHeight + GALLERY_OFFSET + ShapeRender.myHeight + BottomControls.myHeight + insets.top + insets.bottom));
    }
}