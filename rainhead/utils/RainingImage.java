package rainhead.utils;

import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 05.06.13
 * Time: 22:33
 * To change this template use File | Settings | File Templates.
 */
public class RainingImage {
    private BufferedImage bufferedImage;
    private String path;

    public RainingImage(BufferedImage bi, String p) {
        bufferedImage = bi;
        path = p;
    }

    public BufferedImage getBufferedImage(){
        return bufferedImage;
    }

    public String getPath(){
        return path;
    }
}
